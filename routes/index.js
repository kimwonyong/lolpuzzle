var lol = require('../routes/lol');
var _ = require('lodash');
var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
  res.redirect('/puzzle');
});

router.get('/puzzle', function(req, res) {
  var id = req.query.id;
  var level = req.query.level || 1;
  var champion = lol.returnChampion(id);
  var leveltext;

  if (level === '1') {
    leveltext = '하';
  } else if (level === '2') {
    leveltext = '중';
  } else if (level === '3') {
    leveltext = '상';
  } else if (level === '4') {
    leveltext = '최상';
  } else {
    leveltext = '하';
  }

  if (!champion) {
    champion = lol.getChampion();
  }

  res.render('index', {
    champion: champion,
    rows: level * 2,
    columns: level * 3,
    leveltext: leveltext
  });
});







module.exports = router;

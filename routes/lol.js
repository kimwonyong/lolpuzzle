var _ = require('lodash');
var CronJob = require('cron').CronJob;
var request = require('request');

var champions = null;

var requestChampions = function() {
  var version_url = 'https://ddragon.leagueoflegends.com/realms/kr.json';
  request(version_url, function(error, response, body) {
    var result = JSON.parse(body);
    var champion_version = result.n.champion;
    var champion_url = 'http://ddragon.leagueoflegends.com/cdn/' + champion_version + '/data/ko_KR/champion.json';
    request(champion_url, function(error, response, body) {
      var result = JSON.parse(body);
      champions = result.data;
    });
  });
};

requestChampions();


new CronJob('00 00 00 * * *', function() {
  requestChampions();
}, null, true);



module.exports = {
  getChampion: function() {
    return _.sample(champions);
  },
  returnChampion: function(id) {
    return champions[id];
  }
};

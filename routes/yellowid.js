// dev: http://180.229.98.158:3000
// pro: http://lolpuzzle.circledragon.co.kr

var lol = require('../routes/lol');
var _ = require('lodash');
var CronJob = require('cron').CronJob;
var request = require('request');
var express = require('express');
var router = express.Router();

// Home Keyboard API
router.get('/keyboard', function(req, res) {
  res.json({
    'type': 'buttons',
    'buttons': ['문제풀기', '개발자갈구기']
  })
});

// 메시지 수신 및 자동응답 API
router.post('/message', function(req, res) {
  var user_key = req.body.user_key;
  var type = req.body.type; // text, photo
  var content = req.body.content;

  switch (content) {
    case '문제풀기':
      res.json({
        'message': {
          'text': '난이도를 선택해주세요.'
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': [
            '상',
            '중',
            '하',
            '최상'
          ]
        }
      });
      break;
    case '다른문제':
      res.json({
        'message': {
          'text': '난이도를 선택해주세요.'
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': [
            '상',
            '중',
            '하',
            '최상'
          ]
        }
      });
      break;
    case '개발자갈구기':
      res.json({
        'message': {
          'text': '오픈채팅을 참여해서 갈궈줘',
          'photo': {
            'url': 'http://lolpuzzle.circledragon.co.kr/img/logo.png',
            'width': 750,
            'height': 750
          },
          'message_button': {
            'label': '오픈채팅',
            'url': 'https://open.kakao.com/o/gpuwbDk'
          }
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': ['처음으로']
        }
      });
      break;
    case '처음으로':
      res.json({
        'message': {
          'text': '처음입니다.'
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': ['문제풀기', '개발자갈구기']
        }
      });
      break;
    case '상':
      var champion = lol.getChampion();
      res.json({
        'message': {
          'text': '문제: ' + champion.name + '\n' + '난이도: ' + content,
          'photo': {
            'url': 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.id + '_0.jpg',
            'width': 1215,
            'height': 717
          },
          'message_button': {
            'label': '문제풀기',
            'url': 'http://lolpuzzle.circledragon.co.kr/puzzle?id=' + champion.id + '&level=3'
          }
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': [
            '다른문제',
            '처음으로'
          ]
        }
      });


      break;
    case '중':
      var champion = lol.getChampion();
      res.json({
        'message': {
          'text': '문제: ' + champion.name + '\n' + '난이도: ' + content,
          'photo': {
            'url': 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.id + '_0.jpg',
            'width': 1215,
            'height': 717
          },
          'message_button': {
            'label': '문제풀기',
            'url': 'http://lolpuzzle.circledragon.co.kr/puzzle?id=' + champion.id + '&level=2'
          }
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': [
            '다른문제',
            '처음으로'
          ]
        }
      });

      break;
    case '하':
      var champion = lol.getChampion();
      res.json({
        'message': {
          'text': '문제: ' + champion.name + '\n' + '난이도: ' + content,
          'photo': {
            'url': 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.id + '_0.jpg',
            'width': 1215,
            'height': 717
          },
          'message_button': {
            'label': '문제풀기',
            'url': 'http://lolpuzzle.circledragon.co.kr/puzzle?id=' + champion.id + '&level=1'
          }
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': [
            '다른문제',
            '처음으로'
          ]
        }
      });
      break;
    case '최상':
      var champion = lol.getChampion();
      res.json({
        'message': {
          'text': '문제: ' + champion.name + '\n' + '난이도: ' + content,
          'photo': {
            'url': 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.id + '_0.jpg',
            'width': 1215,
            'height': 717
          },
          'message_button': {
            'label': '문제풀기',
            'url': 'http://lolpuzzle.circledragon.co.kr/puzzle?id=' + champion.id + '&level=4'
          }
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': [
            '다른문제',
            '처음으로'
          ]
        }
      });
      break;
    default:
      var champion = lol.getChampion();
      res.json({
        'message': {
          'text': '문제: ' + champion.name + '\n' + '난이도: ' + content,
          'photo': {
            'url': 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.id + '_0.jpg',
            'width': 1215,
            'height': 717
          },
          'message_button': {
            'label': '문제풀기',
            'url': 'http://lolpuzzle.circledragon.co.kr/puzzle?id=' + champion.id
          }
        },
        'keyboard': {
          'type': 'buttons',
          'buttons': [
            '다른문제',
            '처음으로'
          ]
        }
      });
  }
});

// 친구 추가 API
router.post('/friend', function(req, res) {
  var user_key = req.body.user_key;
  res.json({
    data: req.params.user_key
  })
});

// 친구 삭제 API
router.delete('/friend/:user_key', function(req, res) {
  var user_key = req.params.user_key;
  res.json({
    data: req.params.user_key
  })
});



//채팅방 나가기 API
router.delete('/chat_room/:user_key', function(req, res) {
  var user_key = req.params.user_key;
  res.json({
    data: req.params.user_key
  })
});

module.exports = router;

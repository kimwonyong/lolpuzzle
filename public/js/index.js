document.body.addEventListener('touchmove', function(e) {
  e.preventDefault();
});

var app = new Vue({
  el: '#app',
  data: {
    isSuccess: false,
    count: 0,
    timer: 0,
    timerInterval: {}
  },
  computed: {

  },
  mounted: function() {
    var that = this;

    that.genPuzzle();

    $(window).resize(function() {
      $('#source_image').snapPuzzle('refresh');
    });
  },
  methods: {
    genPuzzle: function() {
      var that = this;
      that.isSuccess = false;
      
      $('#source_image').snapPuzzle({
        pile: '#pile',
        onComplete: function() {
          that.isSuccess = true;
          that.count++;
          clearInterval(that.timerInterval);
        }
      });

    },
    startPuzzle: function() {
      var that = this;
      $("#backdrop").hide();

      that.timerInterval = setInterval(function () {
        that.timer++;
      }, 1000);
    },
    nextPuzzle: function() {
      location.reload();
    }
  }
})
